
// Holds data about Pokemon types and their effectiveness.
var typeData = [
    { name: "Fire", data: ["X", "X", "O", "", "O"] },
    { name: "Water", data: ["O", "X", "X", "", ""] },
    { name: "Grass", data: ["X", "O", "X", "", ""] },
    { name: "Electric", data: ["", "O", "X", "X", ""] },
    { name: "Ice", data: ["X", "X", "O", "", "X"] }
];

$(document).ready(function() {

    // TODO Question 2 B: Dynamically generate the contents of the table's <thead>
    var head = document.getElementById('table-header');

    var row = head.insertRow();


    var cell = document.createElement('TH');
    cell.textContent="";
    row.appendChild(cell);

    for (var i = 0; i < typeData.length; i++) {
        cell = document.createElement('TH');
        cell.textContent=typeData[i].name;
        row.appendChild(cell);
    }

    // TODO Question 2 C: Dynamically generate the contents of the table's <tbody>
var table = document.getElementById('table-body');

    for (var i = 0; i <typeData.length ; i++) {
        row = table.insertRow();
        hcell = document.createElement('TH');
        hcell.textContent = typeData[i].name
        row.appendChild(hcell);

        for (var j = 0; j < typeData.length; j++) {
            cell = row.insertCell();
            var content = typeData[i].data[j];
            cell.textContent = typeData[i].data[j];
            if(content=="X"){
                cell.setAttribute("style","color:red");
                cell.setAttribute("bgcolor","blue");
            }else if (content=="O"){
                cell.setAttribute("style","color:black");
                cell.setAttribute("bgcolor","purple");
            }else {
                cell.setAttribute("bgcolor","grey");
            }

        }
    }

});