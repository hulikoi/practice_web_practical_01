package question3;

public class PokemonType implements Comparable<PokemonType> {

    private String name;

    private String[] data;

    public PokemonType(String name, String... data) {
        this.name = name;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }

    @Override
    public int compareTo(PokemonType o) {
        return this.name.compareTo(o.name);
    }
}
