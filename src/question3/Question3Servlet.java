package question3;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class Question3Servlet extends HttpServlet {

    private static final PokemonType[] POKEMON_TYPES = {

            new PokemonType("Fire", "X", "X", "O", "", "O"),
            new PokemonType("Water", "O", "X", "X", "", ""),
            new PokemonType("Grass", "X", "O", "X", "", ""),
            new PokemonType("Electric", "", "O", "X", "X", ""),
            new PokemonType("Ice", "X", "X", "O", "", "X"),
    };

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Get the list of all Pokemon types, which we may need to sort
        List<PokemonType> types = new ArrayList<>(Arrays.asList(POKEMON_TYPES));

        // Get the sort order, by reading the "sortOrder" cookie
        String order = getSortOrder(req);

        // Write the current sort order to the "sortOrder" cookie
        saveSortOrder(order, resp);

        String nextOrder = "none";

        // TODO Q3 B) Depending on the value of "order", either sort the "types" list in ascending or descending order, or don't sort it at all.
        // TODO Q3 C) Also, set the nextOrder variable:
        // TODO - If we're currently "asc", set to "desc"
        // TODO - If we're currently "desc", set to "none"
        // TODO - Otherwise, set to "asc".
        if(order.equalsIgnoreCase("acs")){
            Collections.sort(types);
            nextOrder="desc";
        }else if (order.equalsIgnoreCase("desc")){
            Collections.reverse(types);
            nextOrder="none";
        } else {
            nextOrder="acs";
        }


        // *** DO NOT EDIT THIS METHOD BELOW THIS LINE!!!!! ***

        // Render the response. Don't worry too much if you don't understand this line - just trust that it works :)
        req.setAttribute("columnData", POKEMON_TYPES);
        req.setAttribute("rowData", types);
        req.setAttribute("currentOrder", order);
        req.setAttribute("nextOrder", nextOrder);
        req.getRequestDispatcher("WEB-INF/question3.jsp").forward(req, resp);
    }

    private String getSortOrder(HttpServletRequest req) {

        String sortOrder = "none";

        // TODO Q3 A) If the sort order is specified as a request parameter called "sortOrder", return that.
        if(req.getParameter("sortOrder")!=null){
            sortOrder=req.getParameter("sortOrder");
        } else {
            Cookie[] cookies = req.getCookies();
            for (Cookie cookie:cookies
                 ) {
                if(cookie.getName().equalsIgnoreCase("sortOrder")){
                    sortOrder=cookie.getValue();
                }
            }
        }

        // TODO Q3 D) If the sort order isn't specified as a request parameter, try to read it from the "sortOrder" cookie instead.


        return sortOrder;

    }

    private void saveSortOrder(String sortOrder, HttpServletResponse resp) {

        // TODO Q3 E) Create a cookie called "sortOrder" with the supplied value, and add it to the response.
        Cookie sortOrderCookie= new Cookie("sortOrder",sortOrder);
        resp.addCookie(sortOrderCookie);
    }


}
